This page collects some notes taken during the development of the GPU perfetto producer.

## Environment

Compile [Collabora RockPi Linux](https://gitlab.collabora.com/rockpi/linux) using a custom config which enables tracing and panfrost:

```sh
export ARCH="arm64"
export CROSS_COMPILE="aarch64-linux-gnu-"

make defconfig

# Tracing
./scripts/config -e CONFIG_FTRACE
./scripts/config -e CONFIG_FUNCTION_TRACER
./scripts/config -e CONFIG_FUNCTION_GRAPH_TRACER
./scripts/config -e CONFIG_DYNAMIC_TRACEV

# Panfrost
./scripts/config -e CONFIG_CMA
./scripts/config -e CONFIG_DMA_CMA
./scripts/config -m CONFIG_DRM_PANFROST

make -j $(($(nproc)/2 + 1)) bindeb-pkg
```

## GPU perfetto producer

The producer queries the GPU through `drm/panfrost_drm.h`, using performance counters ioctls, which can be enabled by setting a kernel parameter: `modprobe panfrost unstable_ioctls=1`. The ioctl needs a buffer to copy data from kernel to user space. I do not know the exact size this buffer should be. My guess is `32` bits per counter * (`64`) counters per block * (`4`) blocks.

The producer samples performance counters every *n* millisecond. Once the data is available, it sends a protobuf packet to perfetto server. A the beginning, it sends a description of the counters. After that, it sends counters values using the ids set in the description.

## Cross-compilation

Make sure you are compiling with a libc version compatible with the one installed on the target machine.

### Cross-compile perfetto

In order to capture a trace on the RockPi, a perfetto server and data-sources need to be cross-compiled for Linux aarch64. It is not possible to compile on Linux aarch64 as perfetto needs `gn`, which is not compiled for that architecture.

To cross-compile on amd64, using [clang](https://www.youtube.com/watch?v=D47hCV6wpxo), run `./tools/gn args out/arm64` with the following args:

```js
// args.gn
target_os = "linux"
target_cpu = "arm64"
is_debug = false
target_sysroot = "/usr/aarch64-linux-gnu"
target_gcc_toolchain = "/usr"
```

### Cross-compile producers

To cross-compile the producers with cmake, you need to take these steps:

0. Install **arm64** toolchain:

```sh
sudo apt install libc6-arm64-cross gcc-aarch64-linux-gnu g++-aarch64-linux-gnu
```

1. Generate the project setting `CROSS_COMPILE_ARM64`:

```sh
cmake -S. -Bbuild/arm64 -DCROSS_COMPILE_ARM64=ON
```

2. Compile specifying the data-sources as targets

```sh
cmake --build build/arm64 --target producer-gpu producer-weston
```

### Issues

Cross-compilation is not straightforward. Many things can go wrong and solving certain kinds of issues could be problematic for people who do not have experience with the subject.

#### Compiling

```s
../../buildtools/libcxx/include/__config:215:12: fatal error: 'features.h' file not found
```
Make sure `libc6-arm-cross` and `libc6-dev-arm64-cross` are installed.

#### Linking

```s
ld.lld: error: cannot open crtbegin.o: No such file or directory
ld.lld: error: unable to find library -lgcc_eh
ld.lld: error: cannot open crtend.o: No such file or directory
```
Clang needs some gcc dependencies, the package to install on debian-based systems is `libgcc-#-dev-arm64-cross`.

#### Running

If you are linking libc dinamically, make sure the version of the library installed on your target is greater than the version used during the cross-compilation.
> Is it possible to statically link everything?

## Perfetto UI

The [web ui](https://ui.perfetto.dev) can be used to load the `trace.protobuf` file which is the result of a tracing session.

I works by performing sql-like queries to collect data to show. GPU counters are stored in a table named `gpu_counter_track`. GPU frequency is shown in a track named `GPU # Frequency`, other counters fall under a track named `CounterTrack`. Perfetto populates the `gpu_counter_track` in `ParseGpuCounterEvent`. It gets the counter descriptor, iterates through its specs and verifies that they all have a counter id and a name. The GPU frequency can be sampled with [Ftrace](https://perfetto.dev/#/ftrace.md), other GPU counters need an ad-hoc data-source.
