This page contains some notes about [perfetto](https://docs.perfetto.dev/#/) and [perf](https://perf.wiki.kernel.org/index.php/Main_Page) (perf_events).

## Perf producer

Perfetto has a built-in perf producer (`traced_perf`) under `src/profiling/perf`, wich connects to the tracing service (`traced`). This producer has a data-source named `linux.perf`, which flow is the following:

- Read perf events with `EventReader`.
- Parse perf sample records (`PERF_RECORD_SAMPLE`).
- Input them to the *callstack unwinding*.

You can build it by running the following command:

```sh
buildtools/linux64/ninja -C out/amd64 traced_perf
```

The data-source will call `perf_event_open`, which requires root permissions, therefore run it with sudo:

```sh
sudo out/amd64/traced_perf
```

