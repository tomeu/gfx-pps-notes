This page collects some notes taken during the development of the Weston perfetto producer.

## Perfetto producer

The producer communicates with weston through its [debug protocol](https://www.collabora.com/news-and-blog/blog/2019/04/24/weston-debugging-and-tracing-on-the-fly/). The debug protocol uses a file descriptor to write timeline events, therefore `TimelineDataSource` uses a [pipe](https://github.com/angrave/SystemProgramming/wiki/Pipes,-Part-1:-Introduction-to-pipes) to read data as it comes from weston. This data contains json-formatted objects which, once parsed, are sent to the tracing service (`traced`) through a `TracePacket`.

## JSON timeline

Weston objects like outputs (wo) and surfaces (ws) are referred by id numbers. The timeline debug protocol, emits json objects to define them. These weston objects are stored as they are parsed, and used to associate timepoints to their *track*.

## Track-events

The data-source creates a different track for each weston output or surface, and uses [track-events](
https://docs.perfetto.dev/#/app-instrumentation?id=track-events), which represent time-bounded operations on a timeline, for weston timepoints. Tracks are grouped by weston outputs.

According to the timepoint type, a track-event can be of one of this types:

- Begin of a slice
- End of a slice
- Instant event

Instant events are not completely supported by perfetto yet, therefore the the producer creates a slice of `10` us as a workaround.

![weston-trace](images/weston-trace.png)

## Intermediate events

In order to create slices for Perfetto, the data source uses pairs of begin/end intermediate event types. Here is a complete list of these events, with the corresponding weston timeline points which generate them.

| Intermediate event       | Weston timeline point       |
|--------------------------|-----------------------------|
| CORE_COMMIT_DAMAGE       | "core_commit_damage"        |
| CORE_FLUSH_DAMAGE        | "core_flush_damage"         |
| CORE_REPAINT_BEGIN       | "core_repaint_begin"        |
| CORE_REPAINT_END         | "core_repaint_posted"       |
| CORE_KMS_FLIP_BEGIN      | "core_repaint_posted"       |
| CORE_KMS_FLIP_END        | "core_repaint_finished"     |
| CORE_REPAINT_ENTER_LOOP  | "core_repaint_enter_loop"   |
| CORE_REPAINT_EXIT_LOOP   | "core_repaint_exit_loop"    |
| CORE_REPAINT_REQ_BEGIN   | "core_repaint_req"          |
| CORE_REPAINT_REQ_END     | "core_repaint_finished" or "core_repaint_begin" \*  |
| CORE_REPAINT_DELAY_BEGIN | "core_repaint_finished" \*  |
| CORE_REPAINT_DELAY_END   | "core_repaint_begin" \*     |
| RENDERER_GPU_BEGIN       | "render_gpu_begin"          |
| RENDERER_GPU_END         | "render_gpu_end"            |
| VBLANK_BEGIN             | "core_repaint_finished" \** |
| VBLANK_END               | "core_repaint_finished" \** |

\* only if a "core_repaint_req" is pending

\** carrying a "vblank"
