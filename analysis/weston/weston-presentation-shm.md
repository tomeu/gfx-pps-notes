# Weston presentation-shm analysis

This page contains an analysis of a trace of [weston-presentation-shm](https://gitlab.freedesktop.org/wayland/weston/-/blob/master/clients/presentation-shm.c) taken with [producer-weston](https://gitlab.collabora.com/Fahien/gfx-pps-notes/-/blob/master/producer/weston.md).

## Feedback mode

Typing `weston-presentation-shm -f` will run the client in feedback mode, which makes it to paint as response to the frame callback. [Download the captured trace](trace/timeline-presentation-f.protobuf).

![wesgr-feedback](https://4.bp.blogspot.com/-oDe_MtCX2DY/VNTWl2ARYCI/AAAAAAAAAMM/j_CgQOiKbrU/s1600/after-framecb.png)

![perfetto-feedback](image/timeline-presentation-f.png)

## Feedback idle mode

Typing `weston-presentation-shm -i` will run the client in feedback-idle mode, whith a sleeping period of 1 second between frames. [Download the captured trace](trace/timeline-presentation-i.protobuf).

![wesgr-feedback-idle](image/timeline-presentation-i.svg)

![perfetto-feedback-idle](image/timeline-presentation-i.png)

## Feedback throttle mode

Typing `weston-presentation-shm -d 1` will run the client in feedback mode, whith a sleeping period of 1 ms between frames. [Download the captured trace](trace/timeline-presentation-f-throttle.protobuf).

![wesgr-feedback-throttle](image/timeline-presentation-f-throttle.svg)

![perfetto-feedback-throttle](image/timeline-presentation-f-throttle.png)

## Presentation mode

Typing `weston-presentation-shm -p` will run the client in low-latency presentation mode, which makes it to wait for a frame to be shown before painting again. [Download the captured trace](trace/timeline-presentation-p.protobuf).

![wesgr-presentation](https://3.bp.blogspot.com/-i_MvryvcVQ0/VNTWl_qlE3I/AAAAAAAAAMY/ebB31D09BJk/s1600/after-lowlat.png)

![perfetto-presentation](image/timeline-presentation-p.png)

## Presentation idle mode

Typing `weston-presentation-shm -p -d 1000` will run the client in presentation mode, whith a sleeping period of 1 second between frames. [Download the captured trace](trace/timeline-presentation-p-idle.protobuf).

![wesgr-presentation-idle](image/timeline-presentation-p-idle.svg)

![perfetto-presentation-idle](image/timeline-presentation-p-idle.png)

## Presentation throttle mode

Typing `weston-presentation-shm -p -d 1` will run the client in presentation mode, whith a sleeping period of 1 ms between frames. [Download the captured trace](trace/timeline-presentation-p-throttle.protobuf).

![wesgr-presentation-throttle](image/timeline-presentation-p-throttle.svg)

![perfetto-presentation-throttle](image/timeline-presentation-p-throttle.png)

