This page contains an analysis of a trace taken with the [gpu producer](gpu) of [glmark2-es2-wayland](https://github.com/glmark2/glmark2).

You can [download the trace](https://share.collabora.com/index.php/s/ByNfbJGzajsnpZD)(WIP) and open it with the [online Perfetto UI](https://ui.perfetto.dev/).

The benchmark, run with its default values, shows different scenes, changing scene after 10 seconds.

```js
=======================================================
    glmark2 2017.07
=======================================================
    OpenGL Information
    GL_VENDOR:     panfrost
    GL_RENDERER:   panfrost
    GL_VERSION:    OpenGL ES 2.0 Mesa 19.3.0-devel
=======================================================
[build] use-vbo=false: FPS: 357 FrameTime: 2.801 ms
[build] use-vbo=true: FPS: 477 FrameTime: 2.096 ms
[texture] texture-filter=nearest: FPS: 645 FrameTime: 1.550 ms
[texture] texture-filter=linear: FPS: 638 FrameTime: 1.567 ms
[texture] texture-filter=mipmap: FPS: 637 FrameTime: 1.570 ms
[shading] shading=gouraud: FPS: 337 FrameTime: 2.967 ms
[shading] shading=blinn-phong-inf: FPS: 319 FrameTime: 3.135 ms
[shading] shading=phong: FPS: 508 FrameTime: 1.969 ms
[shading] shading=cel: FPS: 294 FrameTime: 3.401 ms
[bump] bump-render=high-poly: FPS: 297 FrameTime: 3.367 ms
[bump] bump-render=normals: FPS: 550 FrameTime: 1.818 ms
[bump] bump-render=height: FPS: 748 FrameTime: 1.337 ms
[effect2d] kernel=0,1,0;1,-4,1;0,1,0;: FPS: 913 FrameTime: 1.095 ms
[effect2d] kernel=1,1,1,1,1;1,1,1,1,1;1,1,1,1,1;: FPS: 259 FrameTime: 3.861 ms
[pulsar] light=false:quads=5:texture=false: FPS: 1104 FrameTime: 0.906 ms
[desktop] blur-radius=5:effect=blur:passes=1:separable=true:windows=4: FPS: 107 FrameTime: 9.346 ms
[desktop] effect=shadow:windows=4: FPS: 394 FrameTime: 2.538 ms
[buffer] columns=200:interleave=false:update-dispersion=0.9:update-fraction=0.5:update-method=map: FPS: 151 FrameTime: 6.623 ms
[buffer] columns=200:interleave=false:update-dispersion=0.9:update-fraction=0.5:update-method=subdata: FPS: 150 FrameTime: 6.667 ms
[buffer] columns=200:interleave=true:update-dispersion=0.9:update-fraction=0.5:update-method=map: FPS: 181 FrameTime: 5.525 ms
[ideas] speed=duration: FPS: 171 FrameTime: 5.848 ms
[jellyfish] <default>: FPS: 129 FrameTime: 7.752 ms
[terrain] <default>: FPS: 18 FrameTime: 55.556 ms
[shadow] <default>: FPS: 225 FrameTime: 4.444 ms
[refract] <default>: FPS: 58 FrameTime: 17.241 ms
[conditionals] fragment-steps=0:vertex-steps=0: FPS: 1051 FrameTime: 0.951 ms
[conditionals] fragment-steps=5:vertex-steps=0: FPS: 726 FrameTime: 1.377 ms
[conditionals] fragment-steps=0:vertex-steps=5: FPS: 1001 FrameTime: 0.999 ms
[function] fragment-complexity=low:fragment-steps=5: FPS: 517 FrameTime: 1.934 ms
[function] fragment-complexity=medium:fragment-steps=5: FPS: 748 FrameTime: 1.337 ms
[loop] fragment-loop=false:fragment-steps=5:vertex-steps=5: FPS: 729 FrameTime: 1.372 ms
[loop] fragment-steps=5:fragment-uniform=false:vertex-steps=5: FPS: 731 FrameTime: 1.368 ms
[loop] fragment-steps=5:fragment-uniform=true:vertex-steps=5: FPS: 357 FrameTime: 2.801 ms
=======================================================
                                  glmark2 Score: 470 
=======================================================
```

The scenes with a low frame time usually use less L2 cache external reads.

## Terrain

It makes sense to analyse the scene achieving the worst performance.

By looking at the trace, you can see that `glmark2-es2-wayland` is idle most of the time, that means the bottle-neck is the GPU.

Two important counters are `JS0 usage` and `JS1 usage`:

- JS0 corresponds to fragment shading workloads.
- JS1 is used for compute shaders, vertex shaders, and tiling workloads.

As vertex processing can happen in parallel to fragment processing, we would expect to see JS0 active all the time.

By looking at the screenshot below, you can see that is not the case, as JS0 is idle while JS1 is active. That is probably due to multiple-buffering not enabled.

![Terrain-frame](uploads/e93b92f4563f55127b1e1d0c77001824/Screenshot_2020-03-25_at_21.43.17.png)