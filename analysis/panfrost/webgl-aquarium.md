This page contains an analysis of a trace of [WebGL Aquarium](https://webglsamples.org/aquarium/aquarium.html) taken on `RK3399` with [gfx-pps gpu producer](/Fahien/gfx-pps/-/wikis/gpu).

1. Download the trace:
  - [1 ms sampling period](https://share.collabora.com/index.php/s/45GdbRifz72CJNa)
  - [1 s sampling period](https://share.collabora.com/index.php/s/8WPoNS5iF5FiGt3)

2. Open it with the [online Perfetto UI](https://ui.perfetto.dev/).

The sample was traced with its default values.

## Frame

The screenshot below is from the 1 ms sampling period trace. We can see a frame generated in **69.3 ms**. We can also notice how both GPU and CPU activity occupy respectively the first and the second half of the highlighted area. This suggests that improvements are needed on both sides.

![webgl-aquarium](uploads/f29ceab42197d752900b49baec44bb47/webgl-aquarium.png)

## GPU

Having a look at the 1 s sampling period trace, we see that the GPU is not operating at its full capacity. Given that the GPU runs at 600MHz, we can deduce it is only performing at **22%** of its capacity.

Looking at `JS0` and the  `JS1`, we can verify that the balance is ok, even though we could achieve more with double or triple-buffering:

- Fragment activity is `~75%`
- Vertex-Tiling-Compute activity is `~30%`

![1ms-gpu-activity](uploads/47d214f18cc0ebb3066fca95ab8b893c/1ms-gpu-activity.png)

## Overdrawn

Mali prefers objects drawn front to back, because it can kill early those fragments that are not visible. By looking at the counters in the screenshot, we can notice that the number of [quads that are killed](https://community.arm.com/developer/tools-software/graphics/b/blog/posts/mali-midgard-family-performance-counters#jive_content_id_335_FRAG_QUADS_EZS_KILLED) is **zero**!

Considering that the number of fragment killed during late ZS testing is quite high—about 5 million fragments are discarded every seconds and their computation was wasted—makes me thing that all geometry is drawn back to front.

![overdrawn](uploads/6937166370942da9dc64bd62cb3e5840/overdrawn.png)

## Bandwidth

To calculate the bandwidth in bytes, we perform the following calculation:

( `L2 external reads` + `L2 external writes` ) * bus width

Supposing the GPU has a 16 byte AXI interface, the result would be **8.7 GB/s**. This value is too high for a mobile GPU, where an ideal value should be below **5 GB/s**.

![bandwidth](uploads/43f40749a72d66add2c28cf15f1a421e/bandwidth.png)