This project is using [Gitlab CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)

## Docker image

A docker image with ubuntu is maintained for this project on another repository: [ubuntu-image](https://gitlab.freedesktop.org/Fahien/ubuntu-image), where you can find a `.gitlab-cy.yml` file which builds the image and pushes it onto its own [container registry](https://docs.gitlab.com/ee/user/packages/container_registry/).

### GitLab pipeline

The CI pipeline is divided in the following stages: `x86_64`, `aarch64, `perfetto`, (`test`).

#### x86_64

The first stage needs to run on `x86_64` to generate code from perfetto with `gn`, which does not run on `aarch64`. The job generates artifacts files for the next stage.

#### aarch64

The second stage runs on `aarch64` architecture and builds the various perfetto producers (weston and gpu), together with the sources generated during the previous stage.

#### perfetto

The perfetto stage runs in parallel with aarch64, on a `x86_64` machine, and builds `traced`, traced_probes`, and `perfetto` binaries for both `x86_64` and `aarch64` targets.

#### test

TODO.
