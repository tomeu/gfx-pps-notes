# Gfx-pps notes

In this project you can find notes or documents related to [gfx-pps](https://gitlab.freedesktop.org/Fahien/gfx-pps).

![gfx-pps-logo](image/logo.png)
